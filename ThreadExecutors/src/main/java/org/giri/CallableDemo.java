/*
 * Copyright (c) Nov 20, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.util.concurrent.*;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class CallableDemo {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<String> stringFuture = executorService.submit(new SomeJob());
        try {
            System.out.println( stringFuture.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class SomeJob implements Callable {
    @Override
    public String call() throws Exception {
        for(int i=0;i< 10; i++){
            System.out.println(i);
        }
        return "SUCCESS";
    }
}
