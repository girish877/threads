/*
 * Copyright (c) Nov 19, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.time.LocalDate;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class SingleThreadExecutorDemo {

    public static void main(String[] args) {

        PrintTimeTask printTimeTask1 = new PrintTimeTask(1);
        PrintTimeTask printTimeTask2 = new PrintTimeTask(2);
        PrintTimeTask printTimeTask3 = new PrintTimeTask(3);
        PrintTimeTask printTimeTask4 = new PrintTimeTask(4);

        ExecutorService executorService =Executors.newSingleThreadExecutor();
        executorService.submit(printTimeTask1);
        executorService.submit(printTimeTask2);
        executorService.submit(printTimeTask3);
        executorService.submit(printTimeTask4);

        executorService.shutdown();


    }

}