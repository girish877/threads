/*
 * Copyright (c) Nov 19, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class SingleThreadScheduledExecutorDemo {

    public static void main(String[] args) {

        PrintTimeTask printTimeTask1 = new PrintTimeTask(1);

        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.schedule(printTimeTask1,10, TimeUnit.SECONDS);

        // This is best example for shutdown and shutdownNow
        scheduledExecutorService.shutdown();


    }
}
