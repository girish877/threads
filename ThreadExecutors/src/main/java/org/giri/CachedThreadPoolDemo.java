/*
 * Copyright (c) Nov 20, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class CachedThreadPoolDemo {

    public static void main(String[] args) {

        PrintNumber printNumber1 = new PrintNumber(1);
        PrintNumber printNumber2 = new PrintNumber(2);
        PrintNumber printNumber3 = new PrintNumber(3);
        PrintNumber printNumber4 = new PrintNumber(4);

        //Cahed Thread pool is difficult reproduce.. the main difference between fixedThreadPool is in cahedThreadPool is any thread is idle it can be reused adn also if any thread is idle for 10sec then it moved back to thread pool.
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(printNumber1);
        executorService.submit(printNumber2);
        executorService.submit(printNumber3);
        executorService.submit(printNumber4);

        executorService.shutdown();


    }
}
