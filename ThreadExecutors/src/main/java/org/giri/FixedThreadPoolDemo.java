/*
 * Copyright (c) Nov 19, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class FixedThreadPoolDemo {

    public static void main(String[] args) {

        PrintNumber printNumber1 = new PrintNumber(1);
        PrintNumber printNumber2 = new PrintNumber(2);
        PrintNumber printNumber3 = new PrintNumber(3);
        PrintNumber printNumber4 = new PrintNumber(4);

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(printNumber1);
        executorService.submit(printNumber2);
        executorService.submit(printNumber3);
        executorService.submit(printNumber4);

        executorService.shutdown();


    }
}
