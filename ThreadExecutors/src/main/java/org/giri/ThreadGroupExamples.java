package org.giri;

public class ThreadGroupExamples {
	
	public static void main(String[] args) {
		
		System.out.println(Thread.currentThread().getThreadGroup().getName());
		System.out.println(Thread.currentThread().getThreadGroup().getParent().getName());
		
		ThreadGroup g1 = new ThreadGroup("Loging");
		System.out.println(g1.getParent().getName());
		
		ThreadGroup g2 = new ThreadGroup(g1,"second group");
		System.out.println(g2.getParent().getName());
		
		System.out.println(g1.getMaxPriority());
		
		
		
	}

}
