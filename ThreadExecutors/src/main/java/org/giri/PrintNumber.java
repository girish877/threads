/*
 * Copyright (c) Nov 20, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class PrintNumber implements Runnable {

    private int id;
    public PrintNumber(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for(int i=0; i<10;i++){
            System.out.println("Thread id: "+id+" Printing :"+i);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
    }
}