/*
 * Copyright (c) Nov 19, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class PrintTimeTask implements Runnable {

    private int taskId;

    public PrintTimeTask(int taskId) {
        this.taskId = taskId;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread Started : "+taskId+" Current Time :"+ LocalTime.now());
    }
}

