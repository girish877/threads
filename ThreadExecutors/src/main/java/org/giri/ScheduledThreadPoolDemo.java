/*
 * Copyright (c) Nov 20, 2018. EVRY, All Rights Reserved.
 * This software is the confidential and proprietary information of EVRY.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the License agreement you entered into with EVRY.
 */

package org.giri;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * <b>Description</b>
 * </p>
 *
 * @author $Author:   gireesh_sk
 * @version $Revision: 1.0
 */
public class ScheduledThreadPoolDemo {

    public static void main(String[] args) {

        PrintNumber printNumber1 = new PrintNumber(1);
        PrintNumber printNumber2 = new PrintNumber(2);
        PrintNumber printNumber3 = new PrintNumber(3);
        PrintNumber printNumber4 = new PrintNumber(4);

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.schedule(printNumber1,10, TimeUnit.SECONDS);
        executorService.schedule(printNumber2,10, TimeUnit.SECONDS);
        executorService.schedule(printNumber3,10, TimeUnit.SECONDS);
        executorService.schedule(printNumber4,10, TimeUnit.SECONDS);


        executorService.shutdown();


    }
}
