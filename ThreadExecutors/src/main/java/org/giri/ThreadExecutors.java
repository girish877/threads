package org.giri;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author Girish
 * This class explains Thread Executors Api's - newFixedThreadPool
 *
 */
public class ThreadExecutors {
	
	public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		
		executorService.submit(new Task1(1));
		executorService.submit(new Task1(2));
		executorService.submit(new Task1(3));
		
		executorService.submit(new Task1(1));
		executorService.submit(new Task1(2));
		executorService.submit(new Task1(3));
		
		executorService.shutdown();
		
		System.out.println("Main thread is exisitng.....");
		
		
		
	}

}

class Task1 implements Runnable {
	
	private int taskId;
	
	public Task1(int taskId){
		this.taskId = taskId;
	}

	public void run() {
		System.out.println("##############START##########"+taskId);
		for(int i =0 ; i< 10 ; i++){
			System.out.println("Hi i am from Taks ->"+taskId+" and working on -> "+ i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("##############END##########"+taskId);
		
	}
	
	
	
}
